import React, { useEffect, useState } from "react";

const LearnUseEffect2 = () => {
  let [ar1, setAr1] = useState([1, 2]);

  // the dependency must be always primitive
  // if the dependency is non-primitive make it as primitive use JSON.stringify()

  useEffect(() => {
    console.log("i am useEffect");
  }, [JSON.stringify(ar1)]);
  // '[1,2]' => '[1,2]'

  return (
    <div>
      <button
        onClick={() => {
          setAr1([1, 2]);
        }}
      >
        Change
      </button>
    </div>
  );
};

export default LearnUseEffect2;
