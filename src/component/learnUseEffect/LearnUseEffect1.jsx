import React, { useEffect, useState } from "react";

const LearnUseEffect1 = () => {
  let [count, setCount] = useState(0); //1

  let [count1, setCount1] = useState(100); //101

  // for First Render
  //useEffect code run

  // but from seconde Render
  // it depend on dependency
  // if one of the  dependency change the useEffect run otherwise it does not

  // if there is no dependency
  // useEffect code run for every render

  // we have three cases
  // 1) without dependency
  // 2) with []  dependency
  // 3) with [count1, count2] dependency

  // if the dependency is non-primitive make it as primitive use JSON.stringify()

  console.log("i am component");
  // useEffect(() => {
  //   console.log("i am useEffect");
  // }, [count, count1]);

  useEffect(() => {
    console.log("i am useEffect without dependency");
  });

  return (
    <div>
      count is {count}
      <br></br>
      count1 is {count1}
      <br></br>
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Increment count
      </button>
      <br></br>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        Increment count1
      </button>
    </div>
  );
};

export default LearnUseEffect1;
