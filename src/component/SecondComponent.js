import React from "react";

// to perform javascript operation in any tag we must use {}

const SecondComponent = (props) => {
  let ar1 = [1, 2, 1, 3];

  let uniqueValue = [...new Set(ar1)]; // [... new Set([1,2,1,3])]// [...{1,2,3}]//[1,2,3]

  return (
    <div>
      <p> name is {props.name}</p>
      <p> country is {props.country}</p>
      <p> age is {props.age}</p>
      <div>ram</div>
    </div>
  );
};

export default SecondComponent;
