import React from "react";

const LearnToHandleData = () => {
  //string =
  //number =
  //boolean = boolean will not display in browser
  //null = null will not display in browser
  //undefined  == undefined will not display in browser
  //array    === [] and , will removed
  //object   === you should not add object in tag it will throw error

  //   let a = "nitan";
  //   let a = 1;
  //   let a = false;
  let a;
  //   let a = [1, 2, 3];
  //   let a = { name: "nitan", age: 29 };

  return <div>{a}</div>;
};

export default LearnToHandleData;
