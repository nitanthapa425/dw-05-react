import React, { useState } from "react";
import axios from "axios";

// front data => backenders
// request front to backend  => api hit
const CreateContact = () => {
  let [fullName, setfullName] = useState();
  let [address, setAddress] = useState();
  let [phoneNumber, setPhoneNumber] = useState();
  let [email, setEmail] = useState();
  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          let data = {
            fullName: fullName,
            address: address,
            phoneNumber: phoneNumber,
            email: email,
          };

          axios({
            url: "http://localhost:8000/api/v1/contacts",
            method: "POST",
            data: data,
          });
          
        }}
      >
        <div>
          <label htmlFor="fullName">FullName:</label>
          <input
            id="fullName"
            type="text"
            placeholder="FullName"
            value={fullName}
            onChange={(e) => {
              setfullName(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="address">Address:</label>
          <input
            id="address"
            type="text"
            placeholder="Address"
            value={address}
            onChange={(e) => {
              setAddress(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="phoneNumber">phoneNumber:</label>
          <input
            id="phoneNumber"
            type="number"
            placeholder="phoneNumber"
            value={phoneNumber}
            onChange={(e) => {
              setPhoneNumber(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="email">Email</label>
          <input
            id="email"
            type="email"
            placeholder="Email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          ></input>
        </div>
        <button type="summit">Summit</button>
      </form>
    </div>
  );
};
export default CreateContact;
