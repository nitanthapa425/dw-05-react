import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

// url  = http://localhost:8000/api/v1/contacts/640843baeb8a5a9da8f5e567
// method = PATCH

const ContatcDetails = () => {
  let params = useParams();

  let [details, setDetails] = useState({});

  let getContactDetails = async () => {
    let response = await axios({
      url: `${process.env.REACT_APP_BASEURL}/contacts/${params.id}`,
      method: "PATCH",
    });

    setDetails(response.data.data);

    // console.log(response.data.data);
  };

  useEffect(() => {
    getContactDetails();
  }, []);

  return (
    <div>
      <p>Address: {details.address}</p>
      <p>Email: {details.email}</p>
      <p>phoneNumber: {details.phoneNumber}</p>
      <p>fullName: {details.fullName}</p>
    </div>
  );
};

export default ContatcDetails;
