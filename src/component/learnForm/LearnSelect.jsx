import React, { useState } from "react";

// county
//nepal
//india
//china
//pakistan

//value
//onChange

const LearnSelect = () => {
  let [country, setCountry] = useState("china"); //palki

  let countryOptions = [
    { label: "Nepali", value: "nepal" },
    { label: "China", value: "china" },
    { label: "Pakistan", value: "pakistan" },
    { label: "India", value: "india" },
  ];
  return (
    <div>
      {/* <select
        value={country}
        onChange={(e) => {
          setCountry(e.target.value); //paki
        }}
      >
        <option value="nepal">Nepal</option>
        <option value="india">India</option>
        <option value="china">China</option>
        <option valu="pakistan">Pakistan</option>
      </select> */}

      <select
        value={country}
        onChange={(e) => {
          setCountry(e.target.value); //paki
        }}
      >
        {countryOptions.map((item, i) => {
          return <option value={item.value}> {item.label}</option>;
        })}
      </select>
    </div>
  );
};

export default LearnSelect;
