import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const ReadContact = () => {
  let navigate = useNavigate();
  let [contacts, setContacts] = useState([]);
  let readAllContact = async () => {
    let response = await axios({
      url: `${process.env.REACT_APP_BASEURL}/contacts`,
      method: "GET",
    });

    setContacts(response.data.data.results);
  };

  useEffect(() => {
    readAllContact();
  }, []);

  return (
    <div>
      {contacts.map((item, i) => {
        return (
          <div style={{ border: "solid red 3px", marginBottom: "10px" }}>
            <p>fullName : {item.fullName}</p>
            <p>email: {item.email}</p>
            <p>phoneNumber: {item.phoneNumber}</p>
            <p>createdAt : {new Date(item.createdAt).toLocaleDateString()}</p>
            <button
              onClick={async () => {
                await axios({
                  url: `${process.env.REACT_APP_BASEURL}/contacts/${item._id}`,
                  method: "DELETE",
                });

                readAllContact();
              }}
            >
              Delete
            </button>

            <button
              onClick={() => {
                navigate(`/contacts/${item._id}`);
              }}
            >
              View
            </button>

            <button
              onClick={() => {
                navigate(`/contacts/update/${item._id}`);
              }}
            >
              Edit
            </button>
          </div>
        );
      })}
    </div>
  );
};

export default ReadContact;
