import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";

// front data => backenders
// request front to backend  => api hit
const UpdateContact = () => {
  let params = useParams();
  let navigate = useNavigate();
  let [fullName, setfullName] = useState();
  let [address, setAddress] = useState();
  let [phoneNumber, setPhoneNumber] = useState();
  let [email, setEmail] = useState();

  let getContactDetails = async () => {
    let response = await axios({
      url: `http://localhost:8000/api/v1/contacts/${params.id}`,
      method: "GET",
    });

    let details = response.data.data;
    setfullName(details.fullName);
    setAddress(details.address);
    setPhoneNumber(details.phoneNumber);
    setEmail(details.email);
  };

  useEffect(() => {
    getContactDetails();
  }, []);

  return (
    <div>
      <form
        onSubmit={async (e) => {
          e.preventDefault();
          let data = {
            fullName: fullName,
            address: address,
            phoneNumber: phoneNumber,
            email: email,
          };
          //           url  = http://localhost:8000/api/v1/contacts/640843baeb8a5a9da8f5e567
          // method = PATCH

          await axios({
            url: `${process.env.REACT_APP_BASEURL}/contacts/${params.id}`,
            method: "PATCH",
            data: data,
          });

          navigate(`/contacts/${params.id}`);
        }}
      >
        <div>
          <label htmlFor="fullName">FullName:</label>
          <input
            id="fullName"
            type="text"
            placeholder="FullName"
            value={fullName}
            onChange={(e) => {
              setfullName(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="address">Address:</label>
          <input
            id="address"
            type="text"
            placeholder="Address"
            value={address}
            onChange={(e) => {
              setAddress(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="phoneNumber">phoneNumber:</label>
          <input
            id="phoneNumber"
            type="number"
            placeholder="phoneNumber"
            value={phoneNumber}
            onChange={(e) => {
              setPhoneNumber(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="email">Email</label>
          <input
            id="email"
            type="email"
            placeholder="Email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          ></input>
        </div>
        <button type="summit">Update</button>
      </form>
    </div>
  );
};
export default UpdateContact;
