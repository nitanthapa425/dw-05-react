import React, { useState } from "react";

const LearnRadioButton = () => {
  //React Fragment
  //male , female , other

  //all has state value
  // but radio button has checked

  let [gender, setGender] = useState("male"); //other
  return (
    <>  
      <form
        onSubmit={(e) => {
          e.preventDefault();
          let data = {
            gender: gender,
          };

          console.log(data);
        }}
      >
        <label htmlFor="male">Male</label>
        <input
          type="radio"
          checked={gender === "male"}
          onChange={(e) => {
            setGender(e.target.value);
          }}
          id="male"
          value="male"
        ></input>

        <label htmlFor="female">Female</label>
        <input
          type="radio"
          id="female"
          checked={gender === "female"}
          value="female"
          onChange={(e) => {
            setGender(e.target.value);
          }}
        ></input>

        <label htmlFor="other">Other</label>
        <input
          type="radio"
          id="other"
          value="other"
          checked={gender === "other"}
          onChange={(e) => {
            setGender(e.target.value);
          }}
        ></input>

        <br></br>

        <button type="submit">Submit</button>
      </form>
    </>
  );
};

export default LearnRadioButton;
