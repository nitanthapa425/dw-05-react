import React from "react";

const MapPractice = () => {
  let ar1 = ["nitan", "ram", "hari"];
  return (
    <div>
      {ar1.map((value, i) => {
        return <div key={i}>my best friend is {value}</div>;
      })}
    </div>
  );
};

export default MapPractice;
