import React from "react";

const LearnOnClick = () => {
  return (
    <div>
      <button
        onClick={() => {
          console.log("my name is nitan");
        }}
      >
        Click me
      </button>
    </div>
  );
};

export default LearnOnClick;
