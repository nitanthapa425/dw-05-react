import React, { useRef } from "react";

//change bg color on click
//focus input on click

const LearnUseRef1 = () => {
  let refref = useRef();

  let ref1 = useRef();

  let nameRef = useRef();
  return (
    <div>
      <p ref={ref1}> This is paragraph1</p>
      <p ref={refref}>This is paragraph2</p>
      <button
        onClick={() => {
          refref.current.style.backgroundColor = "red";
          ref1.current.style.backgroundColor = "green";
        }}
      >
        Change bg of paragraph
      </button>
      <br></br>

      <label htmlFor="name">Name: </label>
      <input ref={nameRef} id="name"></input>
      <br></br>

      <button
        onClick={() => {
          nameRef.current.focus();
        }}
      >
        Focus Input
      </button>
    </div>
  );
};

export default LearnUseRef1;
