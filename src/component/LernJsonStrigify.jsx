import React from "react";

const LernJsonStrigify = () => {
  let ar1 = [1, 2, 3];
  //JSON.stringify()
  let ar1String = JSON.stringify(ar1); //  '[1,2,3]'
  let a = JSON.stringify(1); //  '1'
  let b = JSON.stringify(false); // 'false'

  //JSON.parse()

  let c = JSON.parse(ar1String); //  [1,2,3]
  let d = JSON.parse(a); //  1
  let e = JSON.parse(b); // false

  return <div>LernJsonStrigify</div>;
};

export default LernJsonStrigify;
