import React from "react";

const LearnTernaryOperator = () => {
  // num is less than 39   fail
  // num [40-59]  third division
  //  num [ 60-79]  First division
  //  num [80-100]

  let age = 19;

  let isMarried = false;
  return (
    <div>
      LearnTernaryOperator
      {isMarried ? <div>He is married</div> : <div>He is not married</div>}
      {age === 16 ? (
        <div>His age is 16</div>
      ) : age === 17 ? (
        <div>His age is 17</div>
      ) : age === 18 ? (
        <div>His age is 18</div>
      ) : null}
    </div>
  );
};

export default LearnTernaryOperator;
