import React from "react";
import { useNavigate } from "react-router-dom";

const About = () => {
  let navigate = useNavigate();

  return (
    <div>
      About
      <br></br>
      <button
        onClick={() => {
          navigate("/contact", { replace: true });
        }}
      >
        goto contact
      </button>
    </div>
  );
};

export default About;
