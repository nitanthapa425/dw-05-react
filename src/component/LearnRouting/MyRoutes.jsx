import React from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import Home from "./Home";
import About from "./About";
import Contact from "./Contact";
import LearnDynamicRoute from "./LearnDynamicRoute";
import CreateContact from "../learnForm/CreateContact";
import ReadContact from "../learnForm/ReadContact";
import ContatcDetails from "../learnForm/ContatcDetails";
import UpdateContact from "../learnForm/UpdateContact";

//Enable Routing feature in our app
// difference between a tag and NavLink
// when a is clicked url is change and component will reload
// but when navlink is clicked only url is changed

// attached component to the url

// dynamic routes

// *

// learn url

// www.facebook.com/profile/abc?name=nitan&age=29&isMarried=false
// url = routes + query
// routes = baseUrl + routes parameters
// query = query parameters

// get dynamic routes parameter
// get query parameter
// useNavigation
// parent and child

const MyRoutes = () => {
  return (
    <div>
      <Routes>
        <Route path="/" element={<Home></Home>}></Route>
        <Route path="/about" element={<About></About>}></Route>
        <Route path="/contact" element={<Contact></Contact>}></Route>
        <Route
          path="/contact/:id/id1/:id2"
          element={<LearnDynamicRoute></LearnDynamicRoute>}
        ></Route>
        <Route
          path="/contact/:a/:b/c"
          element={<div>learn dynamic routing 2</div>}
        ></Route>

        <Route
          path="/contact/a"
          element={<div>i am contact/a component</div>}
        ></Route>

        {/* <Route path="/blog" element={<div> Blog Component </div>}></Route>
        <Route
          path="/blog/:id"
          element={<div> Blog ID Component </div>}
        ></Route>
        <Route
          path="/blog/create"
          element={<div> Blog Create Component </div>}
        ></Route>
        <Route
          path="/blog/update/:id"
          element={<div> Blog Update Component </div>}
        ></Route> */}

        <Route
          path="blogs"
          element={
            <div>
              <Outlet></Outlet>
              my name is nitan
            </div>
          }
        >
          <Route index element={<div>this is blog</div>}></Route>
          <Route path=":id" element={<div>Detail page</div>}></Route>

          <Route path="create" element={<div>create blog</div>}></Route>

          <Route
            path="update"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route path=":id" element={<div>this is update page</div>}></Route>
          </Route>
        </Route>

        <Route
          path="contacts"
          element={
            <div>
              <Outlet></Outlet>
            </div>
          }
        >
          <Route
            index
            element={
              <div>
                <ReadContact></ReadContact>
              </div>
            }
          ></Route>
          <Route
            path=":id"
            element={
              <div>
                <ContatcDetails></ContatcDetails>
              </div>
            }
          ></Route>

          <Route
            path="create"
            element={
              <div>
                <CreateContact></CreateContact>
              </div>
            }
          ></Route>

          <Route
            path="update"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route
              path=":id"
              element={
                <div>
                  <UpdateContact></UpdateContact>
                </div>
              }
            ></Route>
          </Route>
        </Route>

        <Route path="*" element={<div>404 page</div>}></Route>
      </Routes>
    </div>
  );
};

export default MyRoutes;

//localhost:3000
//localhost:3000/about
//localhost:3000/contact
