import React from "react";
import { NavLink } from "react-router-dom";

const MyNavLink = () => {
  return (
    <div>
      <NavLink to="/" style={{ marginRight: "30px" }}>
        Home
      </NavLink>
      <NavLink to="/about" style={{ marginRight: "30px" }}>
        About
      </NavLink>
      <NavLink to="/contact" style={{ marginRight: "30px" }}>
        Contact
      </NavLink>
      <NavLink to="/contacts/create" style={{ marginRight: "30px" }}>
        create contacts
      </NavLink>
      <NavLink to="/contacts" style={{ marginRight: "30px" }}>
        read all contacts
      </NavLink>
    </div>
  );
};

export default MyNavLink;
