import React from "react";
import { NavLink, Route, Routes } from "react-router-dom";
import Home from "./Home";
import About from "./About";
import Contact from "./Contact";
import MyNavLink from "./MyNavLink";
import MyRoutes from "./MyRoutes";

//React does not have routing feature

const Routing1 = () => {
  return (
    <div>
      <MyNavLink></MyNavLink>
      <MyRoutes></MyRoutes>
    </div>
  );
};

export default Routing1;
