import React from "react";
import { useSearchParams } from "react-router-dom";

const Contact = () => {
  // let [name, setName] = useState()
  let [queryParameters] = useSearchParams();

  console.log(queryParameters.get("name"));
  console.log(queryParameters.get("age"));
  console.log(queryParameters.get("isMarried"));
  return <div>Contact</div>;
};

export default Contact;
