import React, { useState } from "react";
import LearnUseState11 from "./LearnUseState11";
import LearnUseState12 from "./LearnUseState12";

const LearnUseState13 = () => {
  console.log("i am 13 comp");

  let [count, setCount] = useState(0);
  return (
    <div>
      <LearnUseState11></LearnUseState11>
      <LearnUseState12></LearnUseState12>

      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Increment
      </button>
    </div>
  );
};

export default LearnUseState13;
