import React, { useState } from "react";

//setCount is asynchronous function
// a page will render after executing all code
const LearnUseState10 = () => {
  let [count, setCount] = useState(0);
  console.log(count);
  return (
    <div>
      count is {count}
      <br></br>
      {/* <button
        onClick={() => {
          setCount(1); //0+1 =1
          setCount(0); //0+1 =1
        }}
      >
        Increment1
      </button> */}
      <button
        onClick={() => {
          setCount((pre) => {
            return pre + 1; //0+1=1
          });
          setCount((pre) => {
            return pre + 1; //1+1 =2
          });
        }}
      >
        Increment
      </button>
    </div>
  );
};

export default LearnUseState10;

//component memory
// count =0

//extra memory
// count =2
