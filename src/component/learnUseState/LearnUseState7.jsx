import React, { useState } from "react";

// a component will render
// only if state variable is changed

// if any state variable is update then
//the the component will re execute
//such that the state variable which is updated hold the updated value
//and other state variable hold previous value
//note it is not necessary , a component will render when setName, seAge or set...  is called

const LearnUseState7 = () => {
  let [count1, setCount1] = useState(0); //11
  let [count2, setCount2] = useState(100); //104

  return (
    <div>
      count1 is {count1}
      <br></br>
      count2 is {count2}
      <br></br>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        incrementCount1
      </button>
      <button
        onClick={() => {
          setCount2(count2 + 1);
        }}
      >
        increment count2
      </button>
    </div>
  );
};

export default LearnUseState7;
