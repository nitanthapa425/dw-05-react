import React, { useState } from "react";

const LearnUseState1 = () => {
  // let name = "nitan";
  // name="ram"
  //define variable use useState

  let [name, setName] = useState("nitan");
  return (
    <div>
      {name}
      <button
        onClick={() => {
          setName("ram");
        }}
      >
        Chang name
      </button>
    </div>
  );
};

export default LearnUseState1;
