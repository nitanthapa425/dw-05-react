import React, { useState } from "react";

const LearnUseState4 = () => {
  let [show, setShow] = useState(true); //false

  //show =true   = hide
  // show= false ===show
  return (
    <div>
      {show ? <img src="./favicon.ico"></img> : null}

      <br></br>
      <button
        onClick={() => {
          setShow(!show);
        }}
      >
        {show ? "Hide" : "Show"}
      </button>
    </div>
  );
};

export default LearnUseState4;
