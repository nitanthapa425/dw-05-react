import React, { useState } from "react";

const LearnUseState2 = () => {
  let [count1, setCount1] = useState(0);
  return (
    <div>
      count is {count1}
      <br></br>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        Increment count1
      </button>
      <button
        onClick={() => {
          setCount1(count1 - 1);
        }}
      >
        Decrement count1
      </button>
      <button
        onClick={() => {
          setCount1(0);
        }}
      >
        Reset count
      </button>
    </div>
  );
};

export default LearnUseState2;
