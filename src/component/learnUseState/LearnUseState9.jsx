import React, { useState } from "react";

const LearnUseState9 = () => {
  let [count, setCount] = useState(0);
  return (
    <div>
      count is {count}
      <br></br>
      <button
        onClick={() => {
          //   setCount(count+1);
          setCount(() => {
            return count + 1;
          });
        }}
      >
        Increment
      </button>
    </div>
  );
};

export default LearnUseState9;
