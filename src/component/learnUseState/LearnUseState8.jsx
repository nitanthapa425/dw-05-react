import React, { useState } from "react";

//in non primitive
//A page will render only if a memory location is not same
const LearnUseState8 = () => {
  let [ar1, setAr1] = useState([1, 2]);
  console.log("check");

  return (
    <div>
      <button
        onClick={() => {
          setAr1([1, 2]);
        }}
      >
        Increment
      </button>
    </div>
  );
};

export default LearnUseState8;
