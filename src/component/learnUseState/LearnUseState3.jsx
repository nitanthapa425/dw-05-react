import React, { useState } from "react";

const LearnUseState3 = () => {
  let [show, setShow] = useState(true);
  return (
    <div>
      {show ? <img src="./favicon.ico"></img> : null}

      <br></br>
      <button
        onClick={() => {
          setShow(true);
        }}
      >
        {" "}
        Show
      </button>

      <button
        onClick={() => {
          setShow(false);
        }}
      >
        Hide
      </button>
    </div>
  );
};

export default LearnUseState3;
