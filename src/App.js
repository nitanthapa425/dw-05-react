import FirstComponent from "./FirstComponent";
import LearnInlineCss from "./component/LearnInlineCss";
import LearnOnClick from "./component/LearnOnClick";
import LearnOr from "./component/LearnOr";
import Routing1 from "./component/LearnRouting/Routing1";
import LearnTernaryOperator from "./component/LearnTernaryOperator";
import LearnToHandleData from "./component/LearnToHandleData";
import LernJsonStrigify from "./component/LernJsonStrigify";
import MapPractice from "./component/MapPractice";
import SecondComponent from "./component/SecondComponent";
import StoreTag from "./component/StoreTag";
import CreateContact from "./component/learnForm/CreateContact";
import LearnCheckbox from "./component/learnForm/LearnCheckbox";
import LearnRadioButton from "./component/learnForm/LearnRadioButton";
import LearnSelect from "./component/learnForm/LearnSelect";
import ReadContact from "./component/learnForm/ReadContact";
import LearnUseEffect1 from "./component/learnUseEffect/LearnUseEffect1";
import LearnUseEffect2 from "./component/learnUseEffect/LearnUseEffect2";
import LearnUseRef1 from "./component/learnUseRef/LearnUseRef1";
import LearnUseState1 from "./component/learnUseState/LearnUseState1";
import LearnUseState10 from "./component/learnUseState/LearnUseState10";
import LearnUseState13 from "./component/learnUseState/LearnUseState13";
import LearnUseState2 from "./component/learnUseState/LearnUseState2";
import LearnUseState3 from "./component/learnUseState/LearnUseState3";
import LearnUseState4 from "./component/learnUseState/LearnUseState4";
import LearnUseState6 from "./component/learnUseState/LearnUseState6";
import LearnUseState7 from "./component/learnUseState/LearnUseState7";
import LearnUseState8 from "./component/learnUseState/LearnUseState8";
import LearnUseState9 from "./component/learnUseState/LearnUseState9";

function App() {
  return (
    <div>
      {/* <p>My name is nitan</p>
      <h1>This is heading 1</h1>
      <a href="https://www.youtube.com/" target="_blank">
        youtube
      </a>
      <img src="./bug.PNG" alt="favicon" width="300px" height="300px"></img>
      <FirstComponent></FirstComponent> */}

      {/* <SecondComponent name="nitan" country="nepal" age={29}></SecondComponent> */}
      {/* <LearnOr></LearnOr> */}
      {/* <LearnInlineCss></LearnInlineCss> */}

      {/* <LearnOnClick></LearnOnClick> */}
      {/* <StoreTag></StoreTag> */}
      {/* <MapPractice></MapPractice> */}
      {/* <LearnTernaryOperator></LearnTernaryOperator> */}
      {/* <LearnToHandleData></LearnToHandleData> */}
      {/* <LearnUseState1></LearnUseState1> */}
      {/* <LearnUseState2></LearnUseState2> */}
      {/* <LearnUseState3></LearnUseState3> */}
      {/* <LearnUseState4></LearnUseState4> */}
      {/* <LearnUseState6></LearnUseState6> */}
      {/* <LearnUseState7></LearnUseState7> */}
      {/* <LearnUseState8></LearnUseState8> */}
      {/* <LearnUseState9></LearnUseState9> */}
      {/* <LearnUseState10></LearnUseState10> */}
      {/* <LearnUseState13></LearnUseState13> */}
      {/* <LearnUseEffect1></LearnUseEffect1> */}
      {/* <LearnUseEffect2></LearnUseEffect2> */}
      {/* <LernJsonStrigify></LernJsonStrigify> */}

      {/* <LearnSelect></LearnSelect> */}
      {/* <LearnRadioButton></LearnRadioButton> */}
      {/* <LearnCheckbox></LearnCheckbox> */}
      {/* <CreateContact></CreateContact> */}
      {/* <ReadContact></ReadContact> */}

      {/* <LearnUseRef1></LearnUseRef1> */}
      <Routing1></Routing1>
    </div>
  );
}

export default App;
